package sqlgen

import (
	"database/sql/driver"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"strings"
	"testing"
)

type l1NestedField struct {
	id int `sql:"pk"`
}

type l2NestedField struct {
	id      int           `sql:"pk"`
	nested1 l1NestedField `sql:"n3,@table3"`
}

type nestedTables struct {
	nested1 l1NestedField `sql:"@table1"`
	nested2 l2NestedField `sql:"@table2"`
}

type nillableTables struct {
	nested1 l1NestedField  `sql:"@table1!"`
	nested2 *l2NestedField `sql:"@table2"`
}

func TestReadQuery(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i:   -2147483648,
		i64: -9223372036854775808,
		i32: -2147483648,
		i16: -32768,
		i8:  -128,

		u:   4294967295,
		u64: 18446744073709551615,
		u32: 4294967295,
		u16: 65535,
		u8:  255,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}

	query, fields := ReadQuery(data, tName)
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)
	assert.Regexp("^SELECT .* FROM "+tName+" AS a$", query, "Should be a select statement")
	assert.Regexp("SELECT.*"+strings.Join(keys, ",.*")+".*FROM", query, "Should contain the fields in order")
	for idx, key := range keys {
		//Load the ptr
		fieldMap[key].GetPtr()
		assert.Equal(fieldMap[key], fields[idx], "Expect correct field order "+key)
	}

	nested := &nestedTables{}
	query, fields = ReadQuery(nested, tName)
	fieldMap = getFieldData(nested)
	keys = getSortedKeys(fieldMap)

	assert.Regexp("^SELECT .* FROM "+tName+`\s+AS\s+a$`, query, "Should be a select statement")
	assert.Regexp("SELECT.*"+strings.Join(keys, ",.*")+".*FROM", query, "Should contain the fields in order")
	for idx, key := range keys {
		field := fieldMap[key]
		val := field
		if pass, _ := field.Table(); pass {
			//val, _ = getPK(field.GetPtr())
			continue
		}
		assert.Same(val, fields[idx], "Expect correct field order "+key)
	}

}

func TestDeepReadQueryShallow(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i:   -2147483648,
		i64: -9223372036854775808,
		i32: -2147483648,
		i16: -32768,
		i8:  -128,

		u:   4294967295,
		u64: 18446744073709551615,
		u32: 4294967295,
		u16: 65535,
		u8:  255,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}

	query, fields := DeepReadQuery(data, tName)

	assert.Regexp(`^SELECT\s+.+\s+FROM\s+`+tName+`\s+AS\s+a$`, query, "Should be a shallow select statement")

	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)

	assert.Regexp("SELECT.*"+strings.Join(keys, ",.*")+`\s+FROM`, query, "Should contain the fields in order")

	for idx, key := range keys {
		//Load the Ptr
		fieldMap[key].GetPtr()
		assert.EqualValues(fieldMap[key], fields[idx], "Expect correct field value order "+key)
	}
}

func TestDeepReadQuery(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"

	data := &nestedTables{}
	query, fields := DeepReadQuery(data, tName)

	assert.Regexp(`^SELECT\s+.+\s+FROM\s+`+tName+`\s+AS\s+a\s+JOIN table1 AS aa ON aa.id = a.nested1 JOIN table2 AS ab ON ab.id = a.nested2 JOIN table3 AS aba ON aba.id = ab.n3$`, query, "Should be a deep select statement")
	assert.Regexp(`^SELECT.*a.id.*FROM`, query, "Should contain the a.id field")
	assert.Regexp(`^SELECT.*b.id.*FROM`, query, "Should contain the b.id field")
	assert.Regexp(`^SELECT.*ba.id.*FROM`, query, "Should contain the ba.id field")

	assert.Same(&data.nested1.id, fields[0].(*FieldData).GetPtr())
	assert.Same(&data.nested2.id, fields[1].(*FieldData).GetPtr())
	assert.Same(&data.nested2.nested1.id, fields[2].(*FieldData).GetPtr())

	noField := &struct {
		id int `sql:"-"`
	}{
		id: 1000,
	}
	query, fields = DeepReadQuery(noField, tName)
	assert.Equal("", query)
	assert.Nil(fields, "No Fields present in no field")
}

func TestDeepReadQueryNillable(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"

	data := &nillableTables{}
	query, fields := DeepReadQuery(data, tName)

	// Right Join should occur
	assert.Regexp(`^SELECT\s+.+\s+FROM\s+`+
		tName+
		`\s+AS\s+a\s+RIGHT JOIN table1 AS aa ON aa.id = a.nested1 JOIN table2 AS ab ON ab.id = a.nested2 JOIN table3 AS aba ON aba.id = ab.n3$`,
		query,
		"Should be a deep select with RIGHT join",
		data)
	assert.Regexp(`^SELECT.*a.id.*FROM`, query, "Should contain the a.id field")
	assert.Regexp(`^SELECT.*b.id.*FROM`, query, "Should contain the b.id field")
	assert.Regexp(`^SELECT.*ba.id.*FROM`, query, "Should contain the ba.id field")

	assert.Same(&data.nested1.id, fields[0].(*FieldData).GetPtr())
	assert.Same(&data.nested2.id, fields[1].(*FieldData).GetPtr())
	assert.Same(&data.nested2.nested1.id, fields[2].(*FieldData).GetPtr())

	noField := &struct {
		id int `sql:"-"`
	}{
		id: 1000,
	}
	query, fields = DeepReadQuery(noField, tName)
	assert.Equal("", query)
	assert.Nil(fields, "No Fields present in no field")
}

func TestRead(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	answer := &supportedAnonymous{
		i:   -214748364,
		i64: -922337203854775808,
		i32: -214748348,
		i16: -3278,
		i8:  -12,

		u:   429967295,
		u64: 1846744073709551615,
		u32: 429967295,
		u16: 6555,
		u8:  25,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}
	data := &supportedAnonymous{}
	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)

	answerMap := getFieldData(answer)

	answers := make([]driver.Value, len(answerMap))
	count := 0
	// To ensure fields come in the right order
	for _, key := range keys {
		fieldDatum := answerMap[key]
		answers[count] = fieldDatum.GetPtr()
		count++
	}

	// 2 Rows to test multiple results
	rows := mock.NewRows(keys).
		AddRow(answers...).
		AddRow(answers...)

	mock.ExpectQuery(`SELECT\s+.*` + strings.Join(keys, ",.*") + `\s+FROM\s+` + table + `\s+AS\s+a$`).WithArgs().WillReturnRows(rows)
	results, err := Read(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(2, len(results), "All rows should be read")

	assert.Equal(answer, results[0], "Result 1 should match the answer")
	assert.Equal(answer, results[1], "Result 2 should match the answer")

	mock.ExpectQuery(`SELECT\s+.*` + strings.Join(keys, ",.*") + `\s+FROM\s+` + table + `\s+AS\s+a$`).WithArgs().WillReturnError(errors.New("query blew up"))
	_, err = Read(tx, data, table, "")
	assert.Error(err, "Query failure should cause method failure")
}

// Should basically pass the same tests as Read
func TestDeepReadSimple(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	answer := &supportedAnonymous{
		i:   -214748364,
		i64: -922337203854775808,
		i32: -214748348,
		i16: -3278,
		i8:  -12,

		u:   429967295,
		u64: 1846744073709551615,
		u32: 429967295,
		u16: 6555,
		u8:  25,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}
	data := &supportedAnonymous{}
	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)

	answerMap := getFieldData(answer)

	answers := make([]driver.Value, len(answerMap))
	count := 0
	// To ensure fields come in the right order
	for _, key := range keys {
		fieldDatum := answerMap[key]
		answers[count] = fieldDatum.GetPtr()
		count++
	}

	// 2 Rows to test multiple results
	rows := mock.NewRows(keys).
		AddRow(answers...).
		AddRow(answers...)

	mock.ExpectQuery(`SELECT\s+.*` + strings.Join(keys, ",.*") + `\s+FROM\s+` + table + `\s+AS\s+a$`).WithArgs().WillReturnRows(rows)
	results, err := DeepRead(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(2, len(results), "All rows should be read")

	assert.Equal(answer, results[0], "Result 1 should match the answer")
	assert.Equal(answer, results[1], "Result 2 should match the answer")
}

func TestReadNested(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	answer := &nestedTables{
		nested1: l1NestedField{
			id: 4645,
		},
		nested2: l2NestedField{
			id: 89768756,
			nested1: l1NestedField{
				id: 4287,
			},
		},
	}
	data := &nestedTables{}

	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)

	answerMap := getFieldData(answer)

	answers := make([]driver.Value, len(answerMap))
	count := 0
	// To ensure fields come in the right order
	for _, key := range keys {
		fieldDatum := answerMap[key]
		val := fieldDatum.GetPtr()
		if pass, _ := fieldDatum.Table(); pass {
			val = getPK(val).GetPtr()
		}
		answers[count] = val
		count++
	}

	// 2 Rows to test multiple results
	rows := mock.NewRows(keys).
		AddRow(answers...).
		AddRow(answers...)

	mock.ExpectQuery(`SELECT\s+.*` + strings.Join(keys, ",.*") + `\s+FROM\s+` + table + `\s+AS\s+a$`).WithArgs().WillReturnRows(rows)
	results, err := Read(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(2, len(results), "All rows should be read")

	assert.NotEqual(answer, results[0], "Result 1 should not match the answer")
	assert.NotEqual(answer, results[1], "Result 2 should not match the answer")

	r := results[0].(*nestedTables)
	assert.Equal(answer.nested1, r.nested1, "Nested1 copied into the first result")
	assert.Equal(answer.nested2.id, r.nested2.id, "Nested2 PK matches because its only 1 level deep")
	assert.NotEqual(answer.nested2, r.nested2, "Nested2 doesn't match because it goes deeper")
	assert.NotEqual(answer.nested2.nested1, r.nested2.nested1, "Nested2.Nested1 does not match because its too deep")

	r = results[1].(*nestedTables)
	assert.Equal(answer.nested1, r.nested1, "Nested1 copied into the first result")
	assert.Equal(answer.nested2.id, r.nested2.id, "Nested2 PK matches because its only 1 level deep")
	assert.NotEqual(answer.nested2, r.nested2, "Nested2 doesn't match because it goes deeper")
	assert.NotEqual(answer.nested2.nested1, r.nested2.nested1, "Nested2.Nested1 does not match because its too deep")
}

// Should do better than the normal Read
func TestDeepReadNested(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	answer := &nestedTables{
		nested1: l1NestedField{
			id: 24724323,
		},
		nested2: l2NestedField{
			id: 897654,
			nested1: l1NestedField{
				id: 7846,
			},
		},
	}
	data := &nestedTables{}

	table := "EXAMPLE"

	_, answerFields := DeepReadQuery(answer, "")

	answers := make([]driver.Value, len(answerFields))
	// To ensure fields come in the right order
	for idx, answerField := range answerFields {
		answers[idx] = answerField.(*FieldData).GetPtr()
	}
	keys := []string{"a.id", "b.id", "ba.id"}

	// 2 Rows to test multiple results
	rows := mock.NewRows(keys).
		AddRow(answers...).
		AddRow(answers...)

	mock.ExpectQuery(`SELECT\s+.*` + strings.Join(keys, ",.*") + `*\s+FROM\s+` + table + `\s+AS\s+a\s+JOIN`).WithArgs().WillReturnRows(rows)
	results, err := DeepRead(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(2, len(results), "All rows should be read")

	assert.Equal(answer, results[0], "First result matches")
	assert.Equal(answer, results[1], "Second Result matches")

	mock.ExpectQuery(`SELECT\s+.*` + strings.Join(keys, ",.*") + `*\s+FROM\s+` + table + `\s+AS\s+a\s+JOIN`).WithArgs().WillReturnError(errors.New("Query blew up"))
	_, err = DeepRead(tx, data, table, "")
	assert.Error(err, "Deep read should throw if query throws")
}

func TestDeepReadNestedNillable(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const table = "SAMPLE"

	rows := mock.NewRows([]string{"n1.id", "n2.id", "n2.n1.id"}).
		AddRow(1, nil, nil).
		AddRow(2, 1, 1)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a` +
		`\s+RIGHT JOIN`).WithArgs().WillReturnRows(rows)

	data := &nillableTables{}
	results, err := DeepRead(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(len(results), 2, "All Results returned")
	answer := results[0].(*nillableTables)
	assert.Equal(answer.nested1.id, 1, "ID should set properly")
	assert.Nil(answer.nested2, "Nil PK should have triggered a nil list here")
	// Non Nil
	answer = results[1].(*nillableTables)
	assert.Equal(answer.nested1.id, 2, "ID should set properly")
	assert.NotNil(answer.nested2, "PK not being Nil should keep this not nil")
	assert.Equal(answer.nested2.id, 1, "n2.ID should set properly")
	assert.Equal(answer.nested2.nested1.id, 1, "n2.n1.ID should set properly")
}

func TestReadNestedNillable(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const table = "SAMPLE"

	rows := mock.NewRows([]string{"n1.id", "n2.id"}).
		AddRow(1, nil).
		AddRow(2, 1)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a`).WithArgs().WillReturnRows(rows)

	data := &nillableTables{}
	results, err := Read(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(len(results), 2, "All Results returned")
	answer := results[0].(*nillableTables)
	assert.Equal(answer.nested1.id, 1, "ID should set properly")
	assert.Nil(answer.nested2, "Nil PK should have triggered a nil list here")
	// Non Nil
	answer = results[1].(*nillableTables)
	assert.Equal(answer.nested1.id, 2, "ID should set properly")
	assert.NotNil(answer.nested2, "PK not being Nil should keep this not nil")
	assert.Equal(answer.nested2.id, 1, "n2.ID should set properly")
}

type nillable struct {
	id           int
	nillableInt  *int
	nillableUint *uint
}

func TestReadNillable(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()

	const table = "SAMPLE"

	rows := mock.NewRows([]string{"id", "nillableInt", "nillableUint"}).
		AddRow(1, nil, uint(14)).
		AddRow(2, 1, nil)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a`).WithArgs().WillReturnRows(rows)

	data := &nillable{}
	results, err := Read(tx, data, table, "")
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Read expected")
	assert.Equal(2, len(results), "All Results returned")
	answer := results[0].(*nillable)
	assert.Equal(1, answer.id, "ID should set properly")
	assert.Nil(answer.nillableInt, "Null should be nil")
	assert.NotNil(answer.nillableUint, "Not nil shouldn't be nil")
	assert.Equal(uint(14), *answer.nillableUint, "Should be a uint proper")
	answer = results[1].(*nillable)
	assert.Equal(2, answer.id, "ID should set properly")
	assert.NotNil(answer.nillableInt, "Not nil shouldn't be nil")
	assert.Equal(1, *answer.nillableInt, "Should be an int proper")
	assert.Nil(answer.nillableUint, "Null should be nil")
}

func TestSearch(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const table = "SAMPLE"

	// No filters
	answers, rows := nillableResults(4)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a$`).WithArgs().WillReturnRows(rows)
	results, err := Search(tx, &nillable{}, table, []string{})
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

	// No Where
	answers, rows = nillableResults(4)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a ORDER BY id$`).WithArgs().WillReturnRows(rows)
	results, err = Search(tx, &nillable{}, table, []string{"ORDER BY id"})
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

	// 1 Arg filter
	answers, rows = nillableResults(4)
	arg1 := 348997
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a WHERE id = \$1$`).WithArgs(arg1).WillReturnRows(rows)
	results, err = Search(tx, &nillable{}, table, []string{"id = $1"}, arg1)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

	// Multi-Arg filter
	answers, rows = nillableResults(4)
	arg2 := 978432
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+`+table+`\s+AS\s+a WHERE arg1 = \$1 AND arg2 = \$2$`).WithArgs(arg1, arg2).WillReturnRows(rows)
	results, err = Search(tx, &nillable{}, table, []string{"arg1 = $1", "arg2 = $2"}, arg1, arg2)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")
}

func TestDeepSearch(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const table = "SAMPLE"

	// No filters
	answers, rows := nillableResults(4)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a$`).WithArgs().WillReturnRows(rows)
	results, err := DeepSearch(tx, &nillable{}, table, []string{})
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

	// No Where
	answers, rows = nillableResults(4)
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a ORDER BY id$`).WithArgs().WillReturnRows(rows)
	results, err = DeepSearch(tx, &nillable{}, table, []string{"ORDER BY id"})
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

	// 1 Arg filter
	answers, rows = nillableResults(4)
	arg1 := 348997
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+` + table + `\s+AS\s+a WHERE id = \$1$`).WithArgs(arg1).WillReturnRows(rows)
	results, err = DeepSearch(tx, &nillable{}, table, []string{"id = $1"}, arg1)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

	// Multi-Arg filter
	answers, rows = nillableResults(4)
	arg2 := 978432
	mock.ExpectQuery(`SELECT\s+.*\s+FROM\s+`+table+`\s+AS\s+a WHERE arg1 = \$1 AND arg2 = \$2$`).WithArgs(arg1, arg2).WillReturnRows(rows)
	results, err = DeepSearch(tx, &nillable{}, table, []string{"arg1 = $1", "arg2 = $2"}, arg1, arg2)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Query expected")
	assert.ElementsMatch(results, answers, "All results should come out properly")

}

func nillableResults(count int) ([]*nillable, *sqlmock.Rows) {
	rows := sqlmock.NewRows([]string{"id", "nillableInt", "nillableUint"})
	out := make([]*nillable, count)
	for i := 0; i < count; i++ {
		out[i] = &nillable{
			id: rand.Int(),
		}
		rows.AddRow(out[i].id, out[i].nillableInt, out[i].nillableUint)
	}
	return out, rows
}
