module gitlab.com/nullandvoid/sqlgen.git

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/stretchr/testify v1.4.0
)
