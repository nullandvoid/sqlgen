package sqlgen

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"
)

const MAXDEPTH = 100

func DeepSearch(tx *sql.Tx, template interface{}, table string, filters []string, args ...interface{}) ([]interface{}, error) {
	where := ""
	if len(filters) > 0 {
		if len(args) > 0 {
			where = "WHERE " + strings.Join(filters, " AND ")
		} else {
			where = strings.Join(filters, ",")
		}
	}
	return DeepRead(tx, template, table, where, args...)
}

func Search(tx *sql.Tx, template interface{}, table string, filters []string, args ...interface{}) ([]interface{}, error) {
	where := ""
	if len(filters) > 0 {
		if len(args) > 0 {
			where = "WHERE " + strings.Join(filters, " AND ")
		} else {
			where = strings.Join(filters, ",")
		}
	}
	return Read(tx, template, table, where, args...)
}

func Read(tx *sql.Tx, s interface{}, table string, whereClause string, args ...interface{}) ([]interface{}, error) {
	if tx == nil {
		return nil, errors.New("transaction must not be nil")
	}
	if isNil(s) {
		return nil, errors.New("interface must not be nil")
	}
	q1, _ := ReadQuery(s, table)
	q1 += " " + whereClause
	rows, err := tx.Query(q1, args...)
	if err != nil {
		return nil, err
	}
	out := make([]interface{}, 0)
	defer rows.Close()
	for rows.Next() {
		// Elem required because we expect a PTR
		sType := reflect.TypeOf(s).Elem()
		clone := reflect.Indirect(reflect.New(sType)).Addr().Interface()
		_, cloneFields := ReadQuery(clone, table)
		err = rows.Scan(cloneFields...)
		if err == nil {
			out = append(out, clone)
		}
	}
	return out, err
}

func DeepRead(tx *sql.Tx, s interface{}, table string, whereClause string, args ...interface{}) ([]interface{}, error) {
	if tx == nil {
		return nil, errors.New("transaction must not be nil")
	}
	if isNil(s) {
		return nil, errors.New("interface must not be nil")
	}
	q1, _ := DeepReadQuery(s, table)
	q1 += " " + whereClause
	rows, err := tx.Query(q1, args...)
	if err != nil {
		return nil, err
	}
	out := make([]interface{}, 0)
	defer rows.Close()
	for rows.Next() {
		// Elem required because we expect a PTR
		sType := reflect.TypeOf(s).Elem()
		clone := reflect.Indirect(reflect.New(sType)).Addr().Interface()
		_, cloneFields := DeepReadQuery(clone, table)
		err = rows.Scan(cloneFields...)
		if err == nil {
			out = append(out, clone)
		}
	}
	return out, err
}

func DeepReadQuery(s interface{}, table string) (string, []interface{}) {
	return deepReadQuery(s, table, MAXDEPTH)
}

func ReadQuery(s interface{}, table string) (string, []interface{}) {
	return deepReadQuery(s, table, 1)
}

func deepReadQuery(s interface{}, table string, maxDepth int) (string, []interface{}) {
	argArrays := getArgArrays(s, "a", maxDepth)
	if len(argArrays) == 0 ||
		(len(argArrays) == 1 && argArrays[0].Count == 0) {
		return "", nil
	}
	// Perform a count to limit memory usage
	fieldCount := 0
	for _, arg := range argArrays {
		fieldCount += arg.Count
	}
	seen := 0

	// Remove 1 field per join because we don't want to get both columns
	fields := make([]interface{}, fieldCount-len(argArrays)+1)
	fieldNames := make([]string, fieldCount-len(argArrays)+1)
	joinSQL := "FROM"
	for i, arg := range argArrays {

		if i == 0 {
			joinSQL += " " + table + " AS " + arg.Alias
		} else if i >= maxDepth {
			break
		} else {
			myTable := fmt.Sprintf("%s AS %s", arg.Table, arg.Alias)
			if arg.JoinType != "" {
				joinSQL += fmt.Sprintf(" %s", arg.JoinType)
			}
			joinSQL += fmt.Sprintf(" JOIN %s ON %s.%s = ", myTable, arg.Alias, arg.PK)
			if arg.ParentAlias != "" {
				joinSQL += arg.ParentAlias + "."
			}
			joinSQL += arg.ParentFK
		}
		for idx, fieldDatum := range arg.Fields {
			if fieldDatum.Skippable() {
				continue
			}
			isTable, _ := fieldDatum.Table()
			if isTable {
				continue
			}
			fields[seen] = fieldDatum.GetScanner()
			var name string
			if arg.Alias != "" {
				name = arg.Alias + "."
			}
			name += arg.Names[idx]
			fieldNames[seen] = name
			seen++
		}
	}

	query := "SELECT " +
		strings.Join(fieldNames, ",") + " " +
		joinSQL
	return query, fields
}
