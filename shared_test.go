package sqlgen

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type supportedAnonymous struct {
	i   int
	i64 int64
	i32 int32
	i16 int16
	i8  int8

	u   uint
	u64 uint64
	u32 uint32
	u16 uint16
	u8  uint8

	f64 float64
	f32 float32

	s string

	b bool
}

func TestGetSortedFields(t *testing.T) {
	assert := assert.New(t)
	data := &supportedAnonymous{}
	fields := getFieldData(data)
	answer := []string{"b", "f32", "f64", "i", "i16", "i32", "i64", "i8", "s", "u", "u16", "u32", "u64", "u8"}
	for idx, s := range getSortedKeys(fields) {
		assert.Equal(answer[idx], s)
	}
}

func TestContains(t *testing.T) {
	assert := assert.New(t)
	for _, lookup := range skippableKeywords {
		pass := contains(skippableKeywords, lookup)
		assert.True(pass, "Should be available in the skippable")
	}
}

func TestContainsBeginning(t *testing.T) {
	assert := assert.New(t)
	tKey := "@LouisVouTon"
	pass, match := containsBeginning([]string{TABLE}, tKey)
	assert.True(pass, "Should be caught")
	assert.Equal(TABLE, match, "Should return the symbol")

	pass, match = containsBeginning([]string{"LouisVouTon"}, tKey)
	assert.False(pass, "Should not match")
	assert.Equal("", match, "Should return the empty symbol")
}

func TestContainsMatch(t *testing.T) {
	assert := assert.New(t)
	tKey := "@LouisVouTon"
	pass, match := containsMatch([]string{tKey}, TABLE)
	assert.True(pass, "Should be caught")
	assert.Equal(tKey, match, "Should return the symbol")

	pass, match = containsMatch([]string{TABLE}, tKey)
	assert.False(pass, "Should match")
	assert.Equal("", match, "Should return the empty symbol")
}
