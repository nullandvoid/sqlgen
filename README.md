# sql-gen

![status](https://gitlab.com/nullandvoid/sqlgen/badges/master/pipeline.svg)
![coverage](https://gitlab.com/nullandvoid/sqlgen/badges/master/coverage.svg?job=gotest)

Go Project for SQL CRUD based on golang tags.

## Syntax
Information to this module is passed primarily through "tagging" struct fields. The requisite tag for this package is "sql". When tagging a field, it is neccessary for the field renaming to occur first, or else the tag will potentially be ignored.

Example:
```
type nestedField struct {
  id    int     `sql:"pk"`
  name  string
}

type serializedStruct struct {
  id      int         `sql:"prime,pk,auto"`
  nested  nestedField `sql:"nested,@table1"`
}

example := &serializedStruct{
  id: 90784,
  nested: nestedField{
    id: 98075433,
  }
}
// Note: TableName must be passed in as there is no way to tag the struct to know about it's table
Create(db, example, "TableName")

// Run SQL: INSERT INTO TableName (nested) VALUES (98075433);
```

## Keywords
These are keywords within the tagging space. Fields cannot be named using these keywords. When used, they have various effects to change the meaning behind things.

| Tag Keyword | Usage |
| ----------- | ----- |
| pk          | Marks a field as the Primary Key. Updates will default to using this field as the WHERE filter. When using a nested object, the internal objects PK field will be set as if it existed in the parent during a shallow read |
| auto        | Ignore during Create/Update0. If the field is also a PK, it will be updated during a Create |
| @.\*        | Table marker, only valid on nested Fields. Used during DeepReads so that during a DeepRead, we know which table to join against |
| -           | Ignore this field when serialized |

## Field Name
Field names are managed via tagging the field in the struct. The name MUST be the first field, and MUST NOT be part of the keywords list. If no name is found, the SQL field is presumed to be the same as the field Name.

Sample renaming `id` to `newname`
```
type example struct {
  id int `sql:"newname"`
}
```
## Operations
### Create
Runs an insert against the database. Does NOT recurse onto nested fields. The FK is presumed to be the primary key of a nested object.
Usage:
```
Create(db, example, "Example")
```

### Read
Runs a select against the database. There are two forms, DeepRead and Read. DeepReads will attempt to join internal nested objects marked with the table tag.  Reads will return an array of "interface" so a type cast will need to be performed on individual results.
Usage:
```
results := Read(db, example, "Example", "WHERE id = $1", example.id)
shallowResult := rasults[0].(exampleStruct)
results = DeepRead(db, example, "Example")
deepResult := results[0].(exampleStruct)
```

#### RIGHT JOINs
The library supports right joins now. To use a right join, merely mark the required struct with a !and the library will configure the query to run with a RIGHT JOIN instead of the typical JOIN. In the event that null fields are returned because of the RIGHT JOIN, it will be handled like typical NULL fields EXCEPT when the null is on the primary key. If the primary key of the right joined struct is null, the entire struct pointer will be set to nil in the resultant struct.

Example:
```
type inner struct {
  inner_id int `sql:"pk"`
}

type outter struct {
  id int `sql:"pk"`
  nested *inner `sql:"@TABLE2!"`
}
```

will result in a query like:
```
SELECT a.id, aa.inner_id FROM {TABLE} AS a RIGHT JOIN TABLE2 AS aa JOIN a.nested = aa.inner_id
```

And if the `inner_id` came back null, you would get { 0 nil } as your resultant struct.

#### NULL Fields
If the Database returns a nil field, one of three things can occur.

1. Exception due to the field not being nillable
2. Field will be set to nil (this is the case for flat pointers)
3. Parent Field will be set to nil (this is the case for nested structs)

In case 1, the generator cannot handle nil's into non pointer fields. It will return a scan error.

In case 2, the pointer is a nillable field, so the sql scan operation can reliably set the field to nil without losing meaning

In case 3, the the struct containing the pointer will be set to nil IF the primary key is being set to nil. This happens regardless of the PK being a pointer or not. This means that shallow reads will result in nil structs if there is a null returned for its field.


### Update
Runs an update against the database. This does not support a deep variant. When filters are applied, it will automatically assume you mean equality and will create a string such as "field = $3". If no filter is provided and the struct has a Primary Key, it will automatically use the primary key as part of the filters. Auto fields are ignored for update.
Usage:
```
Update(db, example, "Example", "field1")
```

### Delete
Runs a delete against the database. Does NOT recurse onto nested fields. The primary key of a nested object. If no filters are provided it will try to delete everything in a table. All filters are assumed to be "Equality" filters.
Usage:
```
Delete(db, example, "Example", "id")
```

## Known Limitations:
### DB Support
Currently, all queries compile to actual query strings. This repository basically only supports PostGre Servers 9+ because of it. The "returning" keyword isn't allowed in MySQL, and this is the feature that forces the SQL version.
### Autodetected Field Names are as is
Autodetected field names will NOT wrap their name in the backtick character for postgre interpretation. This means that the autogenerated fields will always be mapped to their lowecase variant in PostGreSQL.



## Comprehensive Examples Ripped from another project:

### Example object where the table is joined against another object:
```golang
type Version struct {
	ID          int    `json:"id" sql:"id,pk,auto"`
	Name        string `json:"name" sql:"name"`
	Description string `json:"description" sql:"description"`
	Published   bool   `json:"published" sql:"published"`
}

type Race struct {
	Name        string  `json:"name" sql:"name,pk"`
	PatronGod   string  `json:"god" sql:"patron_god"`
	Description string  `json:"description" sql:"description"`
	Version     Version `json:"version" sql:"version,@VERSION"`
}

```

### Example Abstract usage of methods:
```golang

func (tx *Tx) Create(template interface{}, table string) error {
	if isNil(template) {
		err := errors.New("Create Can't work on nil")
		return err
	}
	err := sqlgen.Create(tx.transaction, template, table)
	return err
}

func (tx *Tx) Update(template interface{}, table string, filterFields ...string) error {
	if isNil(template) {
		err := errors.New("Update Can't work on nil")
		return err
	}
	tx.logger.PrintDebugAtLayer(1, fmt.Sprintf("Update %s with %v filters", table, filterFields))
	err := sqlgen.Update(tx.transaction, template, table, filterFields...)
	return err
}

func (tx *Tx) Upsert(template interface{}, table string, filtersFields ...string) error {
	if isNil(template) {
		err := errors.New("Upsert Can't work on nil")
		return err
	}
	err := sqlgen.Upsert(tx.transaction, template, table, filtersFields...)
	return err
}

func (tx *Tx) Delete(template interface{}, table string, filterFields ...string) error {
	if isNil(template) {
		err := errors.New("Delete Can't work on nil")
		return err
	}
	err := sqlgen.Delete(tx.transaction, template, table, filterFields...)
	return err
}

func (tx *Tx) List(template interface{}, table string, filters []string, args ...interface{}) []interface{} {
	if isNil(template) {
		err := errors.New("List Can't work on nil")
		return make([]interface{}, 0)
	}
	output, err := sqlgen.DeepSearch(tx.transaction, template, table, filters, args...)
	return output
}

func (tx *Tx) ListShallow(template interface{}, table string, filters []string, args ...interface{}) []interface{} {
	if isNil(template) {
		err := errors.New("ListShallow Can't work on nil")
		return make([]interface{}, 0)
	}
	output, err := sqlgen.Search(tx.transaction, template, table, filters, args...)
	return output
}


```