package sqlgen

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"testing"
)

var _database *sql.DB

func TxSetup(t *testing.T) (*sql.Tx, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when stubbing database", err)
	}
	_database = db
	mock.ExpectBegin()
	tx, err := db.Begin()
	if err != nil {
		_database.Close()
		t.Fatalf("an error '%s' was not expected when stubbing database", err)
	}
	return tx, mock
}

func CleanUp() {
	_database.Close()
}
