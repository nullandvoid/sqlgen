package sqlgen

import (
	"reflect"
	"strings"
)

type argArray struct {
	Names       []string
	Fields      []*FieldData
	Count       int
	Alias       string
	Table       string
	PK          string
	ParentFK    string
	ParentAlias string
	JoinType    string
}

func getPK(s interface{}) *FieldData {
	fields := getFieldData(s)
	for _, field := range fields {
		if field.IsPK() {
			return field
		}
	}
	return nil
}

func getArgArrays(s interface{}, alias string, depth int) []*argArray {
	const charset = "abcdefghijklmnopqrstuvwxyz"
	recursions := 0
	fields := getTagNames(s)
	sortedKeys := getSortedKeys(fields)
	output := make([]*argArray, 1)
	pkfield := getPK(s)
	pkfieldName := ""
	if pkfield != nil {
		pkfieldName = pkfield.GetTaggedName()
	}
	current := argArray{
		Names:  make([]string, len(sortedKeys)),
		Fields: make([]*FieldData, len(sortedKeys)),
		Alias:  alias,
		Table:  "",
		PK:     pkfieldName,
	}
	output[0] = &current
	for idx, key := range sortedKeys {
		if key == PKFIELD {
			continue
		}
		field := fields[key]
		// Skip nested if this is a shallow read or we don't have a table to link to
		if pass, tableName := field.Table(); pass {
			if depth > 1 {
				tableKey := alias + string(charset[recursions])
				recursions++
				deepArrays := getArgArrays(field.GetPtr(), tableKey, depth-1)
				if tableName[len(tableName)-1] == REQUIRED {
					deepArrays[0].JoinType = "RIGHT"
					deepArrays[0].Table = string(tableName[0 : len(tableName)-1])
				} else {
					deepArrays[0].Table = tableName
				}
				for _, args := range deepArrays {
					if args.Count > 0 {
						if args.ParentFK == "" {
							args.ParentAlias = current.Alias
							args.ParentFK = key
						}
						output = append(output, args)
					}
					if field.IsNillable {
						for _, childField := range args.Fields {
							childField.NillableParent = field
						}
					}
				}
			} else {
				pkfield := getPK(field.GetPtr())
				if pkfield != nil {
					if field.IsNillable {
						pkfield.NillableParent = field
					}
					field = pkfield
				}
			}
		}
		current.Names[idx] = key
		current.Fields[idx] = field
		current.Count++
	}
	return output
}

func getFieldData(s interface{}) map[string]*FieldData {

	reflectValue := reflect.ValueOf(s)

	if reflectValue.Kind() != reflect.Ptr {
		panic(INVALID_PTR)
	}
	v := reflectValue.Elem()
	t := v.Type()
	fieldCount := t.NumField()
	fieldData := make(map[string]*FieldData, fieldCount)
	for i := 0; i < fieldCount; i++ {
		sField := t.Field(i)
		field := v.Field(i)
		tags := strings.Split(sField.Tag.Get("sql"), ",")
		fieldDatum := &FieldData{
			Value: sField,
			Field: field,
			Tags:  tags,
			Name:  sField.Name,
		}

		// When anonymous, flatten the fields to hide that fact from reflection engine
		if sField.Anonymous && fieldDatum.NestedStruct() {
			anonFieldData := getFieldData(fieldDatum.GetStruct())
			for key, data := range anonFieldData {
				fieldData[key] = data
			}
		} else {
			fieldData[fieldDatum.Name] = fieldDatum
		}
	}
	return fieldData
}

func getTagNames(s interface{}) map[string]*FieldData {
	fieldData := getFieldData(s)
	fields := make(map[string]*FieldData, 0)
	for _, fieldDatum := range fieldData {
		if fieldDatum.Skippable() {
			continue
		}
		tags := fieldDatum.Tags
		fieldName := fieldDatum.GetTaggedName()
		if fieldDatum.GetPtr() == nil {
			continue
		}
		fields[fieldName] = fieldDatum
		if contains(tags, PKTAG) {
			fields[PKFIELD] = fieldDatum
		}
	}
	return fields
}

func getFieldValues(s interface{}) map[string]interface{} {
	fieldData := getFieldData(s)
	fields := make(map[string]interface{}, 0)
	for _, fieldDatum := range fieldData {
		if fieldDatum.Skippable() {
			continue
		}
		fieldName := fieldDatum.GetTaggedName()
		val := fieldDatum.GetPtr()
		if val == nil {
			continue
		}
		fields[fieldName] = val
	}
	return fields
}
