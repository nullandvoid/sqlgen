package sqlgen

import (
	"database/sql"
	"errors"
	"strconv"
	"strings"
)

func Update(tx *sql.Tx, s interface{}, table string, filters ...string) error {
	if tx == nil {
		return errors.New("transaction must not be nil")
	}
	if isNil(s) {
		return errors.New("interface must not be nil")
	}
	q1, q1args := UpdateQuery(s, table, filters...)
	_, err := tx.Exec(q1, q1args...)
	return err
}

func updateQueryParts(fields map[string]*FieldData, start int, ignoreFilters bool, filters ...string) ([]string, []string, []interface{}) {
	args := make([]interface{}, 0)
	fieldStrings := make([]string, 0)
	filterStrings := make([]string, 0)
	sortedKeys := getSortedKeys(fields)
	for _, key := range sortedKeys {
		field := fields[key]
		val := field.GetPtr()
		if pass, _ := field.Table(); pass {
			val = getPK(val).GetPtr()
		}
		// If no filter, only allow update on the PK
		if len(filters) == 0 && field.IsPK() {
			filters = append(filters, key)
		} else if field.Auto() && !contains(filters, key) {
			continue
		} else if field.Skippable() {
			continue
		}
		if !ignoreFilters || !contains(filters, key) {
			args = append(args, val)
		}

		equality := field.GetTaggedName() + " = $" + strconv.Itoa(len(args)+start)
		if contains(filters, key) {
			filterStrings = append(filterStrings, equality)
		} else {
			fieldStrings = append(fieldStrings, equality)
		}
	}
	return fieldStrings, filterStrings, args
}

func UpdateQuery(s interface{}, table string, filters ...string) (string, []interface{}) {
	fields := getFieldData(s)
	fieldStrings, filterStrings, args := updateQueryParts(fields, 0, false, filters...)
	query := "UPDATE " + table + " SET " +
		strings.Join(fieldStrings, ", ")
	if len(filterStrings) > 0 {
		query += " WHERE " + strings.Join(filterStrings, " AND ")
	}

	return query, args
}
