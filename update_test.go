package sqlgen

import (
	"database/sql/driver"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUpdateQueryNoFilter(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{}
	query, args := UpdateQuery(data, tName)
	fieldData := getFieldData(data)

	assert.Regexp(`UPDATE\s+`+tName+`\s+SET .* = \$1`, query, "Generic Update of the entire table")
	assert.Equal(len(fieldData), len(args), "All fields are in the args")
	answerArgs := make([]interface{}, 0)
	for key, field := range fieldData {
		assert.Regexp(`SET.*\s+`+key+`\s+=`, query, "Update contains SET for field")
		answerArgs = append(answerArgs, field.GetPtr())
	}
	assert.ElementsMatch(answerArgs, args, "All args should match up")
}

func TestUpdateQueryNoFilterButPK(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	type pkTable struct {
		pkField int `sql:"pk"`
		nonPK   int
	}
	data := &pkTable{
		pkField: 874574,
		nonPK:   8945674,
	}
	query, args := UpdateQuery(data, tName)
	fieldData := getFieldData(data)

	assert.Regexp(`UPDATE\s+`+tName+`\s+SET.*WHERE`, query, "Singular update based on PK")
	assert.Equal(len(fieldData), len(args), "All fields are in the args")
	answerArgs := make([]interface{}, 0)
	assert.Regexp(`SET.*\s+nonPK\s+=.*WHERE`, query, "Update contains SET for nonPK")
	assert.Regexp(`WHERE.*\s+pkField\s+=`, query, "Update contains WHERE for PK")
	answerArgs = append(answerArgs, &data.pkField)
	answerArgs = append(answerArgs, &data.nonPK)
	assert.ElementsMatch(answerArgs, args, "All args should match up")
}

func TestUpdateQueryNestedNoFilter(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &nestedTables{
		nested1: l1NestedField{
			id: 8974,
		},
		nested2: l2NestedField{
			id: 8796422,
			nested1: l1NestedField{
				id: 8795445,
			},
		},
	}
	query, args := UpdateQuery(data, tName)
	fieldData := getFieldData(data)

	assert.Regexp(`UPDATE\s+`+tName+`\s+SET`, query, "Generic Update of the entire table for nested")
	assert.Equal(len(fieldData), len(args), "All fields are in the args for nested")
	answerArgs := make([]interface{}, 0)
	for key, field := range fieldData {
		assert.Regexp(`SET.*\s+`+key+`\s+=`, query, "Update contains SET for field")
		val := field.GetPtr()
		if pass, _ := field.Table(); pass {
			val = getPK(val).GetPtr()
		}
		answerArgs = append(answerArgs, val)
	}
	assert.ElementsMatch(answerArgs, args, "All args should match up")
}

func TestUpdateQueryFilter(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{}
	filters := []string{"i", "i64"}
	query, args := UpdateQuery(data, tName, filters...)
	fieldData := getFieldData(data)

	assert.Regexp(`UPDATE\s+`+tName+`\s+SET.*WHERE`, query, "Generic Update of the entire table")
	assert.Equal(len(fieldData), len(args), "All fields are in the args")
	answerArgs := make([]interface{}, 0)
	for key, field := range fieldData {
		if contains(filters, key) {
			assert.Regexp(`WHERE.*\s+`+key+`\s+=.*`, query, "Update contains Where for filter field")
		} else {
			assert.Regexp(`SET.*\s+`+key+`\s+=.*WHERE`, query, "Update contains SET for field")
		}
		answerArgs = append(answerArgs, field.GetPtr())
	}
	assert.ElementsMatch(answerArgs, args, "All args should match up")
}

func TestUpdateQueryNestedFilter(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &nestedTables{
		nested1: l1NestedField{
			id: 8974,
		},
		nested2: l2NestedField{
			id: 8796422,
			nested1: l1NestedField{
				id: 8795445,
			},
		},
	}
	filters := []string{"nested2"}
	query, args := UpdateQuery(data, tName, filters...)
	fieldData := getFieldData(data)

	answerArgs := make([]interface{}, 0)
	for _, field := range fieldData {
		val := field.GetPtr()
		if pass, _ := field.Table(); pass {
			val = getPK(val).GetPtr()
		}
		answerArgs = append(answerArgs, val)
	}

	assert.Regexp(`UPDATE\s+`+tName+`\s+SET.*WHERE`, query, "Filtered Update of the nested entry")
	assert.Regexp(`WHERE\s+nested2\s+=`, query, "Nested update filters on supplied filter")
	assert.Regexp(`SET\s+nested1\s+=.*WHERE`, query, "Nested update sets non filter field")
	assert.Equal(len(fieldData), len(args), "All fields are in the args")
	assert.ElementsMatch(answerArgs, args, "All args should match up")
}

func TestUpdateQueryAutoFilter(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	type autoKeys struct {
		pkField     int `sql:"auto,pk"`
		autoField   int `sql:"auto"`
		filterField int `sql:"auto"`
		normalField int
	}
	data := &autoKeys{}
	filters := []string{"filterField"}
	query, args := UpdateQuery(data, tName, filters...)
	fieldData := getFieldData(data)

	assert.Regexp(`UPDATE\s+`+tName+`\s+SET.*WHERE`, query, "Generic Update of the entire table")
	assert.Equal(2, len(args), "Filter and normalField should be present")
	answerArgs := make([]interface{}, 0)
	// PK Should be added to the filters in the Update itself
	for key, field := range fieldData {
		if contains(filters, key) {
			assert.Regexp(`WHERE.*\s+`+key+`\s+=.*`, query, "Update contains Where for filter field")
		} else if field.Auto() {
			continue
		} else {
			assert.Regexp(`SET.*\s+`+key+`\s+=.*WHERE`, query, "Update contains SET for field")
		}
		answerArgs = append(answerArgs, field.GetPtr())
	}
	assert.ElementsMatch(answerArgs, args, "All args should match up")
}

func TestUpdate(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	data := &supportedAnonymous{}
	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)
	args := make([]driver.Value, len(fieldMap))
	for idx, key := range keys {
		args[idx] = fieldMap[key].GetPtr()
	}

	mock.ExpectExec(`UPDATE\s+` + table + `\s+SET\s+.*`).WithArgs(args...).WillReturnResult(sqlmock.NewResult(1, 1))

	err := Update(tx, data, table)
	assert.NoError(err, "update should be saved to the DB")
	assert.NoError(mock.ExpectationsWereMet(), "Update expected")

	// Error Test
	mock.ExpectExec(`UPDATE\s+` + table + `\s+SET\s+.*`).WithArgs(args...).WillReturnError(errors.New("Exec exploded"))

	err = Update(tx, data, table)
	assert.Error(err, "Error should cause error to bubble out")
	assert.NoError(mock.ExpectationsWereMet(), "Update expected")

}

func TestUpdateNil(t *testing.T) {
	assert := assert.New(t)
	tx, _ := TxSetup(t)
	defer CleanUp()
	err := Update(tx, nil, "EXAMPLE")
	assert.Error(err, "Nil objects can't be saved to a DB")
}
