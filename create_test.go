package sqlgen

import (
	"database/sql/driver"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestCreateQuery(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i:   -2147483648,
		i64: -9223372036854775808,
		i32: -2147483648,
		i16: -32768,
		i8:  -128,

		u:   4294967295,
		u64: 18446744073709551615,
		u32: 4294967295,
		u16: 65535,
		u8:  255,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}

	query, fields := CreateQuery(data, tName)
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)
	assert.Regexp(`^INSERT\s+INTO\s+`+tName, query, "Should be an insert statement")
	assert.Regexp(`^INSERT\s+INTO\s+`+tName+`\s*\(`+strings.Join(keys, ",")+`\)`, query, "Should contain the fields in order")
	for idx, key := range keys {
		assert.Same(fieldMap[key].GetPtr(), fields[idx], "Expect correct field order "+key)
	}

	nested := &nestedTables{}
	query, fields = CreateQuery(nested, tName)
	fieldMap = getFieldData(nested)
	keys = getSortedKeys(fieldMap)

	assert.Regexp(`^INSERT\s+INTO\s+`+tName, query, "Should be an insert statement")
	assert.Regexp(`^INSERT\s+INTO\s+`+tName+`\s*\(`+strings.Join(keys, ",")+`\)`, query, "Should contain the fields in order")
	for idx, key := range keys {
		field := fieldMap[key]
		val := field.GetPtr()
		if pass, _ := field.Table(); pass {
			val = getPK(field.GetPtr()).GetPtr()
		}
		assert.Same(val, fields[idx], "Expect correct field order "+key)
	}

}

func TestCreateQueryWithAutoPK(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	type example struct {
		id     int `sql:"auto"`
		field2 int
	}
	data := &example{}

	query, fields := CreateQuery(data, tName)
	fieldMap := getFieldData(data)
	assert.Regexp(`^INSERT\s+INTO\s+`+tName, query, "Should be an insert statement")
	assert.Regexp(`^INSERT\s+INTO\s+`+tName+`\s*\(field2\)`, query, "Should contain the field without the AUTO field")
	assert.Same(fieldMap["field2"].GetPtr(), fields[0], "Expect correct field")
	assert.Equal(1, len(fields), "Ignored fields shouldnt appear")
}

func TestCreateQueryIgnore(t *testing.T) {
	assert := assert.New(t)
	type example struct {
		pkField int `sql:"pk,-,auto"`
		auto    int `sql:"-, auto"`
		ignored int `sql:"-"`
		found   int
	}
	tName := "TestCreateQueryIgnore"
	data := &example{}
	query, fields := CreateQuery(data, tName)
	fieldMap := getFieldData(data)
	assert.Regexp(`^INSERT\s+INTO\s+`+tName, query, "Should be an insert statement")

	for _, field := range []string{"pkField", "auto", "ignored"} {
		assert.NotRegexp(`INSERT.+\(.*`+field+`.*\)\s+VALUES\s+\(.*$`, query, "Not expecting field in query: "+field)
	}
	assert.Regexp(`INSERT.+\(.*found.*\)\s+VALUES\s+\(.*$`, query, "Expected 'found' in query")
	assert.Same(fieldMap["found"].GetPtr(), fields[0], "Expect found to be present")
	assert.Equal(1, len(fields), "Ignored fields shouldnt appear")
}

func TestCreate(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	data := &supportedAnonymous{
		i:   -214748364,
		i64: -922337203854775808,
		i32: -214748348,
		i16: -3278,
		i8:  -12,

		u:   429967295,
		u64: 1846744073709551615,
		u32: 429967295,
		u16: 6555,
		u8:  25,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}
	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)

	args := make([]driver.Value, len(fieldMap))
	count := 0
	// To ensure fields come in the right order
	for _, key := range keys {
		fieldDatum := fieldMap[key]
		args[count] = fieldDatum.GetPtr()
		count++
	}

	mock.ExpectExec(`INSERT\s+INTO\s+` + table + `\(` + strings.Join(keys, ",") + `\s*\)\s+VALUES`).WithArgs(args...).WillReturnResult(sqlmock.NewResult(1, 1))
	err := Create(tx, data, table)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Create expected")
}

func TestCreateNested(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	data := &nestedTables{
		nested1: l1NestedField{
			id: 89764,
		},
		nested2: l2NestedField{
			id: 9078,
			nested1: l1NestedField{
				id: 9082,
			},
		},
	}
	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)

	args := make([]driver.Value, len(fieldMap))
	count := 0
	// To ensure fields come in the right order
	for _, key := range keys {
		fieldDatum := fieldMap[key]
		arg := fieldDatum.GetPtr()
		if pass, _ := fieldDatum.Table(); pass {
			arg = getPK(arg).GetPtr()
		}
		args[count] = arg
		count++
	}

	mock.ExpectExec(`INSERT\s+INTO\s+` + table + `\(` + strings.Join(keys, ",") + `\s*\)\s+VALUES`).WithArgs(args...).WillReturnResult(sqlmock.NewResult(1, 1))
	err := Create(tx, data, table)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Create expected")

	mock.ExpectExec(`INSERT\s+INTO\s+` + table + `\(` + strings.Join(keys, ",") + `\s*\)\s+VALUES`).WithArgs(args...).WillReturnError(errors.New("Insert blew up"))
	err = Create(tx, data, table)
	assert.Error(err, "Should reflect failure that caused rollback")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Create expected")
}

func TestCreateAutoPK(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	type AutoPK struct {
		id     int `sql:"pk,auto"`
		field1 int
		field2 string
	}
	data := &AutoPK{
		field1: 84,
		field2: "Something",
	}
	table := "EXAMPLE"
	fieldMap := getFieldData(data)
	keys := getSortedKeys(fieldMap)
	finalKeys := make([]string, len(keys)-1)

	args := make([]driver.Value, len(keys)-1)
	count := 0
	// To ensure fields come in the right order
	for _, key := range keys {
		fieldDatum := fieldMap[key]
		arg := fieldDatum.GetPtr()
		if pass, _ := fieldDatum.Table(); pass {
			arg = getPK(arg).GetPtr()
		}
		if fieldDatum.Auto() {
			continue
		}
		args[count] = arg
		finalKeys[count] = key
		count++
	}

	answerID := 57

	mock.ExpectQuery(`INSERT\s+INTO\s+` + table + `\(` + strings.Join(finalKeys, ",") + `\s*\)\s+VALUES.*RETURNING\s+id`).WithArgs(args...).WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(answerID))
	err := Create(tx, data, table)
	assert.NoError(err, "Should succeed")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Create expected")
	assert.Equal(answerID, data.id, "primary key should be autto incremented")

	mock.ExpectQuery(`INSERT\s+INTO\s+` + table + `\(` + strings.Join(finalKeys, ",") + `\s*\)\s+VALUES.*RETURNING\s+id`).WithArgs(args...).WillReturnError(errors.New("Query blew up"))
	err = Create(tx, data, table)
	assert.Error(err, "Should return error that caused rollback")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Create expected")
}

func TestCreateNil(t *testing.T) {
	assert := assert.New(t)
	tx, _ := TxSetup(t)
	defer CleanUp()
	err := Create(tx, nil, "EXAMPLE")
	assert.Error(err, "Nil objects can't be saved to a DB")
}
