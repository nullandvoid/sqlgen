package sqlgen

import (
	"database/sql"
	"errors"
	"strings"
)

func Upsert(tx *sql.Tx, s interface{}, table string, keys ...string) error {
	if tx == nil {
		return errors.New("transaction must not be nil")
	}
	if isNil(s) {
		return errors.New("interface must not be nil")
	}
	//Get fields
	fields := getFieldData(s)

	q1, q1args := upsertQuery(fields, table, keys...)
	_, err := tx.Exec(q1, q1args...)
	return err
}

func upsertQuery(fields map[string]*FieldData, table string, keys ...string) (string, []interface{}) {
	createFieldNames, createVString, createArgs := createQueryParts(fields, 0)
	updateFieldStrings, _, updateArgs := updateQueryParts(fields, len(createArgs), true, keys...)

	names := make([]string, len(keys))
	for idx, key := range keys {
		names[idx] = fields[key].GetTaggedName()
	}

	query := "INSERT INTO " +
		table +
		"(" +
		strings.Join(createFieldNames, ",") +
		") VALUES (" +
		strings.Join(createVString, ",") +
		") ON CONFLICT(" +
		strings.Join(names, ",") +
		") DO UPDATE SET " +
		strings.Join(updateFieldStrings, ",")

	args := append(createArgs, updateArgs...)
	return query, args
}
