package sqlgen

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetFieldValuesAnonymous(t *testing.T) {
	assert := assert.New(t)
	data := supportedAnonymous{
		i:   -2147483648,
		i64: -9223372036854775808,
		i32: -2147483648,
		i16: -32768,
		i8:  -128,

		u:   4294967295,
		u64: 18446744073709551615,
		u32: 4294967295,
		u16: 65535,
		u8:  255,

		f64: 1.7976931348623e+308,
		f32: 3.4028234663852e+38,

		s: "sample string",

		b: true,
	}

	c := data
	assert.True(&c != &data, "Expect PTRs to differ for tests")
	fields := getFieldValues(&data)
	assert.EqualValues(&c.i, fields["i"])
	assert.EqualValues(&c.i64, fields["i64"])
	assert.EqualValues(&c.i32, fields["i32"])
	assert.EqualValues(&c.i16, fields["i16"])
	assert.EqualValues(&c.i8, fields["i8"])
	assert.Same(&data.i, fields["i"])
	assert.Same(&data.i64, fields["i64"])
	assert.Same(&data.i32, fields["i32"])
	assert.Same(&data.i16, fields["i16"])
	assert.Same(&data.i8, fields["i8"])

	assert.EqualValues(&c.u, fields["u"])
	assert.EqualValues(&c.u64, fields["u64"])
	assert.EqualValues(&c.u32, fields["u32"])
	assert.EqualValues(&c.u16, fields["u16"])
	assert.EqualValues(&c.u8, fields["u8"])
	assert.Same(&data.u, fields["u"])
	assert.Same(&data.u64, fields["u64"])
	assert.Same(&data.u32, fields["u32"])
	assert.Same(&data.u16, fields["u16"])
	assert.Same(&data.u8, fields["u8"])

	assert.EqualValues(&c.f64, fields["f64"])
	assert.EqualValues(&c.f32, fields["f32"])
	assert.Same(&data.f64, fields["f64"])
	assert.Same(&data.f32, fields["f32"])

	assert.EqualValues(&c.s, fields["s"])
	assert.Same(&data.s, fields["s"])

	assert.EqualValues(&c.b, fields["b"])
	assert.Same(&data.b, fields["b"])
}

func TestGetFieldValuesNamed(t *testing.T) {
	assert := assert.New(t)
	data := struct {
		n int    `sql:"Name1"`
		s string `sql:"Giovanni"`
	}{
		n: 100,
		s: "sadasdetreb",
	}
	fields := getFieldValues(&data)
	assert.NotNil(fields["Name1"])
	assert.Same(&data.n, fields["Name1"])
	assert.NotNil(fields["Giovanni"])
	assert.Same(&data.s, fields["Giovanni"])
}

func TestGetFieldValuesIgnored(t *testing.T) {
	assert := assert.New(t)
	data := struct {
		n int    `sql:"-"`
		s string `sql:"Giovanni"`
		c complex64
	}{
		n: 100,
		s: "asfdbrt",
	}
	fields := getFieldValues(&data)
	assert.Nil(fields[IGNORE], "The ignore key")
	assert.Nil(fields["n"], "renamed, should not show up")
	assert.Nil(fields["c"], "Complex types aren't handled")
	assert.NotNil(fields["Giovanni"])
	assert.Same(&data.s, fields["Giovanni"])
	assert.Equal(1, len(fields), "Ignored shouldn't show up in list", fields)
}

func TestGetFieldValuesIgnoredStruct(t *testing.T) {
	assert := assert.New(t)
	type nested struct {
		inner_id int `sql:"pk"`
	}
	type container struct {
		ignored  nested `sql:"-"`
		included nested
		id       int
	}
	data := container{
		ignored:  nested{},
		included: nested{},
		id:       42897,
	}
	fields := getFieldValues(&data)
	assert.Nil(fields["ignored"], "Ignored field should be ignored")
	assert.Nil(fields[IGNORE], "Ignore Key should not appear")
	assert.NotNil(fields["id"], "Allowed field 'id' should be found")
	assert.NotNil(fields["included"], "Allowed field 'included' should be found")
	assert.Same(&data.id, fields["id"])
	assert.Same(&data.included.inner_id, fields["included"])
	assert.Equal(2, len(fields), "Only included fields should appear")
}

func TestGetFieldValuesStruct(t *testing.T) {
	assert := assert.New(t)
	data := supportedAnonymous{}
	fn := func() { getFieldValues(data) }

	assert.PanicsWithValue(INVALID_PTR, fn)

}

func TestGetFieldValuesNested(t *testing.T) {
	assert := assert.New(t)

	data := struct {
		nested struct {
			PK int `sql:"pk"`
		}
		s string `sql:"Giovanni"`
	}{
		s: "sadas",
		nested: struct {
			PK int `sql:"pk"`
		}{
			PK: 54,
		},
	}

	fields := getFieldValues(&data)
	assert.NotNil(fields["Giovanni"])
	assert.NotNil(fields["nested"])
	assert.Same(&data.s, fields["Giovanni"])
	assert.Same(&data.nested.PK, fields["nested"])
	assert.Equal(2, len(fields), "All fields should be present", fields)
}

func TestGetArgArraysShallow(t *testing.T) {
	assert := assert.New(t)
	data := &supportedAnonymous{}
	prefix := "jkhsad"
	arrays := getArgArrays(data, prefix, 1)

	answerFields := getFieldValues(data)

	assert.Equal(1, len(arrays))
	assert.Equal(prefix, arrays[0].Alias)
	assert.Equal(len(answerFields), len(arrays[0].Fields), "Without a PK, the fields should be the same")
	assert.Equal(len(answerFields), arrays[0].Count)
	assert.Equal(len(arrays[0].Fields), len(arrays[0].Names))
	assert.Equal("", arrays[0].Table, "No table for a root")

	arrays = getArgArrays(data, prefix, MAXDEPTH)
	assert.Equal(1, len(arrays), "No depth without a nested object")
}

func TestGetArgArraysNoTable(t *testing.T) {
	assert := assert.New(t)
	//No Table annotation
	type noTableNested struct {
		nested struct {
			nestedField1 int
		}
		field1 int
	}
	data := &noTableNested{}
	prefix := "dsklajf"
	arrays := getArgArrays(data, prefix, 1)

	assert.Equal(1, len(arrays), "Shallow should still be shallow")

	arrays = getArgArrays(data, prefix, MAXDEPTH)
	assert.Equal(1, len(arrays), "Should still be shallow as no table has been defined")
}

func TestGetArgArraysTable(t *testing.T) {
	assert := assert.New(t)
	//Table annotation
	type tableNested struct {
		nested struct {
			nestedField1 int
		} `sql:"@table1"`
		nested2 struct {
			nestedField1 int
			nested       struct {
				nestedField1 int
			} `sql:"@table3"`
			nested2 struct {
				nestedField1 int
			}
		} `sql:"@table2"`
		znillable *struct {
			nillableField1 int `sql:"pk"`
			nillableField2 int
		} `sql:"@table4"`
		field1 int
		field2 int
	}
	data := &tableNested{}
	prefix := ""
	arrays := getArgArrays(data, prefix, 1)

	assert.Equal(1, len(arrays), "Shallow should still be shallow")
	assert.Equal("", arrays[0].Table, "Root table is root table")

	data = &tableNested{}
	arrays = getArgArrays(data, prefix, MAXDEPTH)
	assert.Equal(5, len(arrays), "All table definitions should show up")
	assert.Equal("", arrays[0].Table, "Root table is root table")
	assert.Equal(5, len(arrays[0].Fields), "All fields are present", arrays[0].Fields)
	assert.Equal(5, len(arrays[0].Names), "All field names are present", arrays[0].Names)

	assert.Equal("table1", arrays[1].Table, "Nested table name shows up in the first array")
	assert.Equal("a", arrays[1].Alias, "Alias should be the first letter of the alphabet")

	assert.Equal("table2", arrays[2].Table, "Nested table name shows up in the second array")
	assert.Equal("b", arrays[2].Alias, "Second nested table has the second prefix")

	assert.Equal("table3", arrays[3].Table, "Nested table name shows up")
	assert.Equal("ba", arrays[3].Alias, "Second Level nested table has the 2 prefixes")

	assert.Equal("table4", arrays[4].Table, "Nested table name shows up in nillable")
	assert.Equal("c", arrays[4].Alias, "Nillable table alias is correct")
	for _, field := range arrays[4].Fields {
		assert.Same(arrays[0].Fields[4], field.NillableParent, "Field should have correct Nillable detail %s", field.Name)
	}
}

func TestGetPK(t *testing.T) {
	assert := assert.New(t)
	noPK := &supportedAnonymous{}
	pkfield := getPK(noPK)

	assert.Nil(pkfield, "No PK found")

	pkVal := "PK Power!"
	PK := struct {
		field1 string `sql:"pk"`
	}{
		field1: pkVal,
	}

	pkfield = getPK(&PK)
	assert.Equal("field1", pkfield.GetTaggedName())
	assert.EqualValues(&pkVal, pkfield.GetPtr())
}

func TestGetFieldData(t *testing.T) {
	assert := assert.New(t)

	type nested struct {
		inner_id int `sql:"pk"`
		unused   int
	}
	type container struct {
		ignored  nested `sql:"-"`
		included nested
		id       int `sql:"ID"`
		primeKey int `sql:"prime,pk"`
	}
	data := container{
		ignored:  nested{},
		included: nested{},
		id:       89737,
		primeKey: 40987,
	}

	fieldData := getFieldData(&data)
	assert.Equal(4, len(fieldData), "All Fields should be present")
	assert.True(fieldData["ignored"].Skippable(), "Ignored should be skippable")
	assert.True(!fieldData["included"].Skippable(), "Incldued should not be skippable")
	assert.Equal("ID", fieldData["id"].GetTaggedName(), "Tag name should be correct")
	assert.True(fieldData["primeKey"].IsPK(), "Fields marked PK should be PK")
	assert.Equal("prime", fieldData["primeKey"].GetTaggedName(), "Should be able to name a field that has a pk on it")
	assert.Same(&data.primeKey, fieldData["primeKey"].GetPtr(), "primeKey Field should match up")
	assert.Same(&data.ignored.inner_id, fieldData["ignored"].GetPtr(), "ignored Field should match up", fieldData["ignored"])
	assert.Same(&data.included.inner_id, fieldData["included"].GetPtr(), "included Field should match up")
	assert.Same(&data.id, fieldData["id"].GetPtr(), "id Field should match up")
}

func TestGetFieldDataAnonymous(t *testing.T) {
	assert := assert.New(t)

	type nested struct {
		inner_id    int
		inner_field int
		extra_field int
	}
	type container struct {
		nested
		id int
	}
	data := &container{}
	data.id = 3276
	data.inner_id = 2896
	data.inner_field = 4896734

	fieldData := getFieldData(data)
	assert.Equal(4, len(fieldData), "All Fields should be present")
	assert.Equal("id", fieldData["id"].GetTaggedName(), "Tag name should be correct")
	assert.Same(&data.inner_id, fieldData["inner_id"].GetPtr(), "inner_id field should flatten to the root")
	assert.Same(&data.inner_field, fieldData["inner_field"].GetPtr(), "inner_fied field should flatten to the root")
	assert.Same(&data.id, fieldData["id"].GetPtr(), "id Field should match up")
}

func TestGetFieldDataNillable(t *testing.T) {
	assert := assert.New(t)

	type nested struct {
		inner_id    int `sql:"pk"`
		inner_field int
		extra_field int
	}
	type container struct {
		id       int
		nillable *nested
	}
	data := &container{}
	data.id = 3276

	fieldData := getFieldData(data)
	assert.Nil(data.nillable, "Nested field should start nil")
	fieldData["nillable"].GetPtr()
	assert.NotNil(data.nillable, "Nested field should no longer be nil")
	assert.Equal(2, len(fieldData), "All Fields plus nested fields should be present", fieldData)
	assert.Equal("id", fieldData["id"].GetTaggedName(), "Tag name should be correct")
	assert.Same(&data.nillable.inner_id, fieldData["nillable"].GetPtr(), "inner_id field should exist as n1")
}

func TestGetFieldDataNillableTable(t *testing.T) {
	assert := assert.New(t)

	type nested struct {
		inner_id    int `sql:"pk"`
		inner_field int
		extra_field int
	}
	type container struct {
		n1 *nested `sql:"@TABLE1"`
		id int
	}
	data := &container{}
	data.id = 3276

	fieldData := getFieldData(data)
	assert.Nil(data.n1, "Nested field should start nil")
	fieldData["n1"].GetPtr()
	assert.NotNil(data.n1, "Nested field should no longer be nil")
	assert.Equal(2, len(fieldData), "All Fields plus nested fields should be present", fieldData)
	assert.Equal("id", fieldData["id"].GetTaggedName(), "Tag name should be correct")
	assert.Same(data.n1, fieldData["n1"].GetPtr(), "field should exist as n1")
}

var result map[string]*FieldData

func BenchmarkGetFieldData(b *testing.B) {
	for i := 0; i < b.N; i++ {
		data := supportedAnonymous{}
		result = getFieldData(&data)
	}
}
