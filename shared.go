package sqlgen

import (
	"regexp"
	"sort"
	"unsafe"
)

const INVALID_PTR = "Can't perform on non ptrs"

const PKTAG = `pk`
const IGNORE = `-`
const TABLE = `@`
const PKFIELD = `<`
const PKVAL = `>`
const AUTOTAG = `auto`
const REQUIRED = '!'

var keywords = []string{
	PKTAG,
	IGNORE,
	PKFIELD,
	PKVAL,
	AUTOTAG,
}

var keyPrefixes = []string{
	TABLE,
}

var skippableKeywords = []string{
	PKVAL,
	PKFIELD,
	IGNORE,
}

func getSortedKeys(fields map[string]*FieldData) []string {
	keys := make([]string, len(fields))
	idx := 0
	for k, _ := range fields {
		if contains(skippableKeywords, k) {
			continue
		}
		keys[idx] = k
		idx++
	}
	keys = keys[0:idx]
	sort.Strings(keys)
	return keys
}

func containsBeginning(slice []string, key string) (bool, string) {
	for _, regex := range slice {
		if matched, _ := regexp.MatchString("^"+regex, key); matched {
			return true, regex
		}
	}
	return false, ""
}

func contains(slice []string, key string) bool {
	for _, s := range slice {
		if key == s {
			return true
		}
	}
	return false
}

func containsMatch(slice []string, match string) (bool, string) {
	for _, s := range slice {
		if matched, _ := regexp.MatchString(match, s); matched {
			return true, s
		}
	}
	return false, ""
}

func isNil(data interface{}) bool {
	return (*[2]uintptr)(unsafe.Pointer(&data))[1] == 0
}
