package sqlgen

import (
	"database/sql"
	"errors"
	"strconv"
	"strings"
)

func Delete(tx *sql.Tx, s interface{}, table string, filters ...string) error {
	if tx == nil {
		return errors.New("transaction must not be nil")
	}
	if isNil(s) {
		return errors.New("interface must not be nil")
	}
	q1, q1args := DeleteQuery(s, table, filters...)
	_, err := tx.Exec(q1, q1args...)
	return err
}

func DeleteQuery(s interface{}, table string, filters ...string) (string, []interface{}) {
	filterStrings := make([]string, 0)
	args := make([]interface{}, 0)
	if len(filters) > 0 {
		fieldData := getFieldData(s)
		for _, filter := range filters {
			field := fieldData[filter]
			if field == nil {
				continue
			}
			val := field.GetPtr()
			if pass, _ := field.Table(); pass {
				val = getPK(val).GetPtr()
			}
			args = append(args, val)
			filterStrings = append(filterStrings, field.GetTaggedName()+" = $"+strconv.Itoa(len(args)))
		}
	}
	whereString := ""
	if len(filterStrings) > 0 {
		whereString += " WHERE " + strings.Join(filterStrings, " AND ")
	}
	query := "DELETE FROM " +
		table +
		whereString

	return query, args
}
