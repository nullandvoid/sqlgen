package sqlgen

import (
	"database/sql"
	"errors"
	"strconv"
	"strings"
)

func Create(tx *sql.Tx, s interface{}, table string) error {
	if tx == nil {
		return errors.New("transaction must not be nil")
	}
	if isNil(s) {
		return errors.New("interface must not be nil")
	}
	var err error
	//Get fields
	fields := getFieldData(s)
	// Get the Primary Key to Query into
	var key string
	var keyArg interface{}
	for _, field := range fields {
		if field.Auto() && field.IsPK() {
			key = field.GetTaggedName()
			keyArg = field.GetScanner()
		}
	}
	q1, q1args := createQuery(fields, table)
	if keyArg != nil {
		q1 += " RETURNING " + key
		err = tx.QueryRow(q1, q1args...).Scan(keyArg)
	} else {
		_, err = tx.Exec(key+q1, q1args...)
	}
	return err
}

func CreateQuery(s interface{}, table string) (string, []interface{}) {
	fields := getFieldData(s)
	return createQuery(fields, table)
}

func createQueryParts(fields map[string]*FieldData, start int) ([]string, []string, []interface{}) {
	sortedKeys := getSortedKeys(fields)
	i := 0
	count := 0
	for _, field := range fields {
		if field.Auto() || field.Skippable() {
			continue
		}
		count++
	}
	fieldNames := make([]string, count)
	args := make([]interface{}, count)
	vString := make([]string, count)
	for _, key := range sortedKeys {
		field := fields[key]
		if field.Auto() || field.Skippable() {
			continue
		}
		val := field.GetArgument()
		fieldNames[i] = field.GetTaggedName()
		args[i] = val
		vString[i] = "$" + strconv.Itoa(i+1+start)
		i++
	}
	return fieldNames, vString, args
}

func createQuery(fields map[string]*FieldData, table string) (string, []interface{}) {
	fieldNames, vString, args := createQueryParts(fields, 0)
	query := "INSERT INTO " +
		table +
		"(" +
		strings.Join(fieldNames, ",") +
		") VALUES (" +
		strings.Join(vString, ",") +
		")"

	return query, args
}
