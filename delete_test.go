package sqlgen

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDeleteQuery(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{}
	query, fields := DeleteQuery(data, tName)

	assert.Regexp(`DELETE\s+FROM\s+`+tName+`\s*$`, query, "Generic Delete of the entire table")
	assert.Equal(0, len(fields), "No filters provided, so no fields needed")
}

func TestDeleteQueryWithFilters(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i: 90754,
	}
	filters := []string{"i", "f64"}
	query, fields := DeleteQuery(data, tName, filters...)
	fieldData := getFieldData(data)
	answerFields := make([]interface{}, 0)
	for _, filter := range filters {
		field := fieldData[filter]
		if field != nil {
			answerFields = append(answerFields, field.GetPtr())
		}
	}

	assert.Regexp(`DELETE\s+FROM\s+`+tName+`\s+WHERE\s+.*`, query, "Delete has a where clause")
	assert.Equal(len(filters), len(fields), "filters provided should match length of fields")
	for _, filter := range filters {
		assert.Regexp(`WHERE.*`+filter+`\s+=\s+\$`, query, "Filter should be in where clause: "+filter)
	}
	for idx, _ := range filters {
		assert.Same(answerFields[idx], fields[idx], "Expect Field data to be the same Ptr")
	}
}

func TestDeleteQueryWithNestedFilters(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &nestedTables{
		nested1: l1NestedField{
			id: 98754,
		},
		nested2: l2NestedField{
			id: 987054,
			nested1: l1NestedField{
				id: 8974435,
			},
		},
	}
	filters := []string{"nested1"}
	query, fields := DeleteQuery(data, tName, filters...)

	assert.Regexp(`DELETE\s+FROM\s+`+tName+`\s+WHERE\s+.*`, query, "Delete has a where clause")
	assert.Equal(len(filters), len(fields), "filters provided should match length of fields")
	assert.Regexp(`WHERE.*`+filters[0]+`\s+=\s+\$`, query, "Filter should be in where clause: "+filters[0])
	assert.Same(&data.nested1.id, fields[0], "Expect Field data to be the same Ptr")
}

func TestDeleteQueryWithBadFilter(t *testing.T) {
	assert := assert.New(t)
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i: 90754,
	}
	filters := []string{"notrealfield"}
	query, fields := DeleteQuery(data, tName, filters...)

	assert.Regexp(`DELETE\s+FROM\s+`+tName+`\s*$`, query, "Delete has no where clause")
	assert.Equal(0, len(fields), "filters provided did not match a real field")
}

func TestDeleteWithFilter(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i:   90754,
		i64: 98074,
	}
	filters := []string{"i", "i64"}

	mock.ExpectExec(`DELETE\s+FROM\s+`+tName+`\s+WHERE\s+i\s+=.*AND\s+i64\s+=`).WithArgs(data.i, data.i64).WillReturnResult(sqlmock.NewResult(1, 1))
	assert.NoError(Delete(tx, data, tName, filters...))
	assert.NoError(mock.ExpectationsWereMet(), "Singular Delete expected")
}

func TestDeleteWithNestedFilter(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const tName = "SAMPLE"
	data := &nestedTables{
		nested1: l1NestedField{
			id: 98754,
		},
		nested2: l2NestedField{
			id: 987054,
			nested1: l1NestedField{
				id: 8974435,
			},
		},
	}
	filters := []string{"nested1"}

	mock.ExpectExec(`DELETE\s+FROM\s+` + tName + `\s+WHERE\s+nested1\s+=.*`).WithArgs(data.nested1.id).WillReturnResult(sqlmock.NewResult(1, 1))
	assert.NoError(Delete(tx, data, tName, filters...))
	assert.NoError(mock.ExpectationsWereMet(), "Singular Delete expected")
}

func TestDeleteWithBadFilter(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()
	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i: 90754,
	}
	filters := []string{"notrealfield"}

	mock.ExpectExec(`DELETE\s+FROM\s+` + tName + `\s*$`).WithArgs().WillReturnResult(sqlmock.NewResult(1, 1))
	assert.NoError(Delete(tx, data, tName, filters...))
	assert.NoError(mock.ExpectationsWereMet(), "Singular Delete expected")
}
