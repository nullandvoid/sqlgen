package sqlgen

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"unsafe"
)

type FieldData struct {
	Value          reflect.StructField
	Field          reflect.Value
	Tags           []string
	Name           string
	ptr            interface{}
	NillableParent *FieldData
	IsNillable     bool
}

func (f FieldData) GetTaggedName() string {
	if f.Tags[0] == "" {
		return f.Name
	} else if matched, _ := containsBeginning(keyPrefixes, f.Tags[0]); matched {
		return f.Name
	} else if contains(keywords, f.Tags[0]) {
		return f.Name
	}
	return f.Tags[0]
}

func (f FieldData) Skippable() bool {
	return contains(skippableKeywords, f.Tags[0])
}

func (f FieldData) Table() (bool, string) {
	matched, table := containsMatch(f.Tags, "^"+TABLE)
	if matched {
		table = table[1:]
	}
	return matched, table
}

func (f FieldData) IsPK() bool {
	return contains(f.Tags, PKTAG)
}

func (f FieldData) Auto() bool {
	matched, _ := containsMatch(f.Tags, "^"+AUTOTAG)
	return matched
}

func (f FieldData) NestedStruct() bool {
	return f.Field.Kind() == reflect.Struct
}

func (f *FieldData) GetPtr() interface{} {
	f.parse()
	return f.ptr
}

func (f *FieldData) GetArgument() interface{} {
	f.parse()
	field := f.Field
	if field.Kind() == reflect.Ptr {
		field = field.Elem()
	}
	if field.Kind() == reflect.Struct {
		if !field.Addr().CanInterface() {
			field = reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem()
		}
		reference := field.Addr().Interface()
		pkfield := getPK(reference)
		return pkfield.GetPtr()
	}
	return f.GetPtr()
}

func (f *FieldData) GetScanner() sql.Scanner {
	f.parse()
	return f
}

func (f *FieldData) parse() {
	if f.ptr != nil {
		return
	}
	field := f.Field

	var val interface{}

	if field.Kind() == reflect.Ptr {
		if !field.IsNil() {
			field = field.Elem()
		}
	}
	switch field.Kind() {
	case reflect.Bool:
		val = (*bool)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Uint:
		val = (*uint)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Int:
		val = (*int)(unsafe.Pointer(field.Addr().Pointer()))

	case reflect.Int8:
		val = (*int8)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Uint8:
		val = (*uint8)(unsafe.Pointer(field.Addr().Pointer()))

	case reflect.Int16:
		val = (*int16)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Uint16:
		val = (*uint16)(unsafe.Pointer(field.Addr().Pointer()))

	case reflect.Int32:
		val = (*int32)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Uint32:
		val = (*uint32)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Float32:
		val = (*float32)(unsafe.Pointer(field.Addr().Pointer()))

	case reflect.Int64:
		val = (*int64)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Uint64:
		val = (*uint64)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Float64:
		val = (*float64)(unsafe.Pointer(field.Addr().Pointer()))

	case reflect.String:
		val = (*string)(unsafe.Pointer(field.Addr().Pointer()))
	case reflect.Struct:
		if !field.Addr().CanInterface() {
			field = reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem()
		}
		val = getStructPtr(f, field)
	case reflect.Ptr:
		if !field.Addr().CanInterface() {
			field = reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem()
		}
		n := reflect.New(f.Value.Type.Elem()).Elem()
		field.Set(n.Addr())

		f.IsNillable = true

		if n.Kind() == reflect.Struct {
			if n.Addr().CanInterface() {
				val = getStructPtr(f, n)
			}
		} else {
			val = n
		}
	}
	f.ptr = val
	return
}

func (f *FieldData) GetStruct() interface{} {
	field := f.Field
	if field.Kind() == reflect.Struct {
		if !field.Addr().CanInterface() {
			field = reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem()
		}
		return field.Addr().Interface()
	}
	return nil
}

func (fd *FieldData) Scan(value interface{}) error {
	field := fd.Field
	field = reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem()
	if value == nil {
		// Has a nillable parent struct AND this is the PK of that struct
		if fd.NillableParent != nil && fd.IsPK() {
			parentField := fd.NillableParent.Field
			parentField = reflect.NewAt(parentField.Type(), unsafe.Pointer(parentField.UnsafeAddr())).Elem()
			//Parent needs to be nilled because we had no PK
			parentField.Set(reflect.Zero(parentField.Type()))
		} else {
			//This field should be "zeroed"
			if field.Kind() == reflect.Ptr {
				field.Set(reflect.Zero(field.Type()))
			} else {
				return errors.New(fmt.Sprintf("Unable to set nil value for %s, unhandled type: %s", fd.Name, field.Kind()))
			}
		}
	} else {
		if field.Kind() == reflect.Struct {
			reference := field.Addr().Interface()
			pkfield := getPK(reference)
			field = pkfield.Field
		}
		// Go to pointed value if we're a pointer
		if field.Kind() == reflect.Ptr {
			field = field.Elem()
		}
		s := asString(value)
		switch field.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			i, _ := strconv.ParseInt(s, 10, field.Type().Bits())
			field.SetInt(i)
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			i, _ := strconv.ParseUint(s, 10, field.Type().Bits())
			field.SetUint(i)
		case reflect.Float64, reflect.Float32:
			f, _ := strconv.ParseFloat(s, field.Type().Bits())
			field.SetFloat(f)
		case reflect.Bool:
			b, _ := strconv.ParseBool(s)
			field.SetBool(b)
		case reflect.String:
			field.SetString(s)
		}
	}
	return nil
}

func asString(val interface{}) string {
	v := reflect.ValueOf(val)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return strconv.FormatInt(v.Int(), 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return strconv.FormatUint(v.Uint(), 10)
	case reflect.Bool:
		return strconv.FormatBool(v.Bool())
	}
	return fmt.Sprintf("%v", val)
}

func getStructPtr(f *FieldData, n reflect.Value) interface{} {
	reference := n.Addr().Interface()
	if pass, _ := f.Table(); pass {
		return reference
	} else {
		pkfield := getPK(reference)
		if pkfield != nil {
			return pkfield.GetPtr()
		}
	}
	return nil
}
