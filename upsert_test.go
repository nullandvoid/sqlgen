package sqlgen

import (
	"database/sql/driver"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestUpsert(t *testing.T) {
	assert := assert.New(t)
	tx, mock := TxSetup(t)
	defer CleanUp()

	const tName = "SAMPLE"
	data := &supportedAnonymous{
		i:   90754,
		i64: 98074,
	}
	keys := []string{"i", "i64"}

	args := make([]driver.Value, 26)
	for i := 0; i < 26; i++ {
		args[i] = sqlmock.AnyArg()
	}

	mock.ExpectExec(`INSERT INTO ` + tName + `.* VALUES .* ON CONFLICT\(` + strings.Join(keys, ",") + `\) DO UPDATE SET .*`).WithArgs(args...).WillReturnResult(sqlmock.NewResult(1, 1))
	assert.NoError(Upsert(tx, data, tName, keys...))
	assert.NoError(mock.ExpectationsWereMet(), "Singular Delete expected")

	mock.ExpectExec(`INSERT INTO ` + tName + `.* VALUES .* ON CONFLICT\(` + strings.Join(keys, ",") + `\) DO UPDATE SET .*`).WithArgs(args...).WillReturnError(errors.New("Upsert blew up"))
	assert.Error(Upsert(tx, data, tName, keys...), "Failure to execute should cause an error")
	assert.NoError(mock.ExpectationsWereMet(), "Singular Delete expected")
}

func TestUpsertNil(t *testing.T) {
	assert := assert.New(t)
	tx, _ := TxSetup(t)
	defer CleanUp()

	const tName = "SAMPLE"
	keys := []string{"i", "i64"}

	assert.Error(Upsert(tx, nil, tName, keys...), "Nil should cause an error")
}
